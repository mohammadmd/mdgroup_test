<?php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;

class ExpenseControllerTest extends ApiTestCase
{
    public function testGetOne(): void
    {
        $response = static::createClient()->request('GET', '/expenses/1');

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains(['status' => 'success']);
    }

    public function testGetOneNotFound(): void
    {
        $response = static::createClient()->request('GET', '/expenses/566646');

        $this->assertResponseStatusCodeSame(404);
        $this->assertJsonContains(['status' => 'error']);
    }

    public function testGetAll(): void
    {
        $response = static::createClient()->request('GET', '/expenses');

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains(['status' => 'success']);
    }

    public function testAdd(): void
    {
        $response = static::createClient()->request('POST', '/expenses', [
            'json' => [
                'description' => 'dummy description',
                'value' => '7.5',
                'expense_type_id' => '3'
            ]
        ]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertJsonContains([
            'status' => 'success',
            'message' => 'Expense has been created successfully!'
        ]);
    }

    public function testAddInvalidExpenseType(): void
    {
        $response = static::createClient()->request('POST', '/expenses', [
            'json' => [
                'description' => 'dummy description',
                'value' => '8',
                'expense_type_id' => '50'
            ]
        ]);

        $this->assertResponseStatusCodeSame(422);
        $this->assertJsonContains([
            'status' => 'error'
        ]);
    }

    public function testAddMissingParameters(): void
    {
        $response = static::createClient()->request('POST', '/expenses', [
            'json' => [
                'description' => 'dummy description',
                'expense_type_id' => '3'
            ]
        ]);

        $this->assertResponseStatusCodeSame(400);
        $this->assertJsonContains([
            'status' => 'error'
        ]);
    }

    public function testUpdate(): void
    {
        $response = static::createClient()->request('PUT', '/expenses/1', [
            'json' => [
                'description' => 'dummy description',
                'value' => '7.5',
                'expense_type_id' => '3'
            ]
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'status' => 'success',
            'message' => 'Expense has been updated successfully!'
        ]);
    }

    public function testUpdateNotFound(): void
    {
        $response = static::createClient()->request('PUT', '/expenses/65456654', [
            'json' => [
                'description' => 'dummy description',
                'value' => '7.5',
                'expense_type_id' => '3'
            ]
        ]);

        $this->assertResponseStatusCodeSame(404);
        $this->assertJsonContains(['status' => 'error']);
    }

    public function testUpdateInvalidExpenseType(): void
    {
        $response = static::createClient()->request('PUT', '/expenses/1', [
            'json' => [
                'description' => 'dummy description',
                'value' => '7.5',
                'expense_type_id' => '665'
            ]
        ]);

        $this->assertResponseStatusCodeSame(422);
        $this->assertJsonContains(['status' => 'error']);
    }

    public function testDelete(): void
    {
        $response = static::createClient()->request('DELETE', '/expenses/1');

        $this->assertResponseStatusCodeSame(204);
    }

    public function testDeleteNotFound(): void
    {
        $response = static::createClient()->request('DELETE', '/expenses/6556456');

        $this->assertResponseStatusCodeSame(404);
        $this->assertJsonContains(['status' => 'error']);
    }
}
