<?php

namespace App\Custom;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

trait ResponseHandler
{
    public static function okWithData($message = 'Ok!', $data = [], $status = Response::HTTP_OK): JsonResponse
    {
        $response = [
            'status' => 'success',
            'message' => $message,
            'data' => $data
        ];

        return new JsonResponse($response, $status);
    }

    public static function created($message = 'Created successfully!', $status = Response::HTTP_CREATED): JsonResponse
    {
        $response = [
            'status' => 'success',
            'message' => $message
        ];

        return new JsonResponse($response, $status);
    }

    /**
     * in case of default status response (204) =>
     * the content will not be sent
     */
    public static function deleted($message = 'Deleted sucessfully!', $status = Response::HTTP_NO_CONTENT): JsonResponse
    {
        $response = [
            'status' => 'success',
            'message' => $message
        ];

        return new JsonResponse($response, $status);
    }

    public static function notFound($message = 'Not found!', $status = Response::HTTP_NOT_FOUND): JsonResponse
    {
        $response = [
            'status' => 'error',
            'message' => $message
        ];

        return new JsonResponse($response, $status);
    }

    public static function badRequest($message = 'Bad request!', $status = Response::HTTP_BAD_REQUEST): JsonResponse
    {
        $response = [
            'status' => 'error',
            'message' => $message
        ];

        return new JsonResponse($response, $status);
    }

    public static function unprocessableEntity($message = 'Unprocessable entity!', $status = Response::HTTP_UNPROCESSABLE_ENTITY): JsonResponse
    {
        $response = [
            'status' => 'error',
            'message' => $message
        ];

        return new JsonResponse($response, $status);
    }

    public static function internalServerError($message = 'Internal server error!', $status = Response::HTTP_INTERNAL_SERVER_ERROR): JsonResponse
    {
        $response = [
            'status' => 'error',
            'message' => $message
        ];

        return new JsonResponse($response, $status);
    }
}
