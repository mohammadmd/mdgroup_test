<?php

namespace App\Controller;

use App\Custom\ResponseHandler;
use App\Repository\ExpenseRepository;
use App\Repository\ExpenseTypeRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ExpenseController extends BaseController
{
    private $expenseRepository;
    private $expenseTypeRepository;

    public function __construct(ExpenseRepository $expenseRepository, ExpenseTypeRepository $expenseTypeRepository)
    {
        $this->expenseRepository = $expenseRepository;
        $this->expenseTypeRepository = $expenseTypeRepository;
    }

    /**
     * @Route("/expenses", name="list_expenses", methods={"GET"})
     */
    public function getAll(): JsonResponse
    {
        $expenses = $this->expenseRepository->findAll();

        $data = [];

        foreach ($expenses as $expense) {
            $data[] = $expense->toArray();
        }

        return ResponseHandler::okWithData('Expenses have been retreived successfully!', $data);
    }

    /**
     * @Route("/expenses/{id}", name="get_expense", methods={"GET"})
     */
    public function getOne($id): JsonResponse
    {
        $expense = $this->expenseRepository->findOneBy(['id' => $id]);

        if (empty($expense)) {
            return ResponseHandler::notFound('Expense not found!');
        }

        $data = $expense->toArray();

        return ResponseHandler::okWithData('Expense has been retreived successfully!', $data);
    }

    /**
     * @Route("/expenses", name="add_expense", methods={"POST"})
     */
    public function add(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $description = $data['description'] ?? null;
        $value = $data['value'] ?? null;
        $expenseTypeId = $data['expense_type_id'] ?? null;
        
        $expenseType = $this->expenseTypeRepository->findOneBy(['id' => $expenseTypeId]);

        if (empty($value) || empty($expenseTypeId)) {
            return ResponseHandler::badRequest('Expecting mandatory parameters!');
        }

        if (empty($expenseType)) {
            return ResponseHandler::unprocessableEntity('Expense Type not found!');
        }

        if (!$this->expenseRepository->save($description, $value, $expenseType)) {
            return ResponseHandler::internalServerError('Expense couldn\'t be created!');
        }

        return ResponseHandler::created('Expense has been created successfully!');
    }

    /**
     * @Route("/expenses/{id}", name="update_expense", methods={"PUT"})
     */
    public function update($id, Request $request): JsonResponse
    {
        $expense = $this->expenseRepository->findOneBy(['id' => $id]);

        if (empty($expense)) {
            return ResponseHandler::notFound('Expense not found!');
        }
        
        $data = json_decode($request->getContent(), true);

        empty($data['description']) ? true : $expense->setDescription($data['description']);
        empty($data['value']) ? true : $expense->setValue($data['value']);

        $expenseTypeId = $data['expense_type_id'];
        
        $expenseType = $this->expenseTypeRepository->findOneBy(['id' => $expenseTypeId]);

        if (empty($expenseType)) {
            return ResponseHandler::unprocessableEntity('Expense Type not found!');
        }

        empty($data['expense_type_id']) ? true : $expense->setExpenseType($expenseType);

        $updatedExpense = $this->expenseRepository->update($expense);

        if ($updatedExpense === false) {
            return ResponseHandler::internalServerError('Expense couldn\'t be updated!');
        }

        return ResponseHandler::okWithData('Expense has been updated successfully!', $updatedExpense->toArray());
    }

    /**
     * @Route("/expenses/{id}", name="delete_expense", methods={"DELETE"})
     */
    public function delete($id): JsonResponse
    {
        $expense = $this->expenseRepository->findOneBy(['id' => $id]);

        if (empty($expense)) {
            return ResponseHandler::notFound('Expense not found!');
        }

        if (!$this->expenseRepository->remove($expense)) {
            return ResponseHandler::internalServerError('Expense couldn\'t be deleted!');
        }

        return ResponseHandler::deleted();
    }
}
