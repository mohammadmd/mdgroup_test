<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Custom\ResponseHandler;

class BaseController extends AbstractController
{
    use ResponseHandler;
}
