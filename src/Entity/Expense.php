<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ExpenseRepository::class)
 */
class Expense
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="float")
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity=ExpenseType::class, inversedBy="expenses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $expense_type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(float $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getExpenseType(): ?ExpenseType
    {
        return $this->expense_type;
    }

    public function setExpenseType(?ExpenseType $expense_type): self
    {
        $this->expense_type = $expense_type;

        return $this;
    }

    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'description' => $this->getDescription(),
            'value' => $this->getValue(),
            'expense_type' => $this->getExpenseType()->toArray()
        ];
    }
}
