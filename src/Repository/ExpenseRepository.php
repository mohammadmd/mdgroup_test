<?php

namespace App\Repository;

use App\Entity\Expense;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Exception;

/**
 * @method Expense|null find($id, $lockMode = null, $lockVersion = null)
 * @method Expense|null findOneBy(array $criteria, array $orderBy = null)
 * @method Expense[]    findAll()
 * @method Expense[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExpenseRepository extends ServiceEntityRepository
{
    private $manager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Expense::class);

        $this->manager = $manager;
    }

    public function save($description, $value, $ExpenseType)
    {
        try {
            $entity = new Expense();

            $entity
                ->setDescription($description)
                ->setValue($value)
                ->setExpenseType($ExpenseType);

            $this->manager->persist($entity);
            $this->manager->flush();
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    public function update(Expense $expense): Expense
    {
        try {
            $this->manager->persist($expense);
            $this->manager->flush();
        } catch (Exception $e) {
            return false;
        }

        return $expense;
    }

    public function remove(Expense $expense)
    {
        try {
            $this->manager->remove($expense);
            $this->manager->flush();
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    // /**
    //  * @return Expense[] Returns an array of Expense objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Expense
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
